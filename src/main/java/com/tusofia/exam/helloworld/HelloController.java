package com.tusofia.exam.helloworld;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.stream.Collectors;

@RestController
public class HelloController {

    @GetMapping("/quick-sort")
    public ResponseEntity<String> getQuickSortedNumbers(@RequestParam(name = "arr") Integer[] intArray) {
        quickSort(intArray, 0, intArray.length - 1);
        return ResponseEntity.ok(Arrays.stream(intArray).map(Object::toString).collect(Collectors.joining(",")));
    }

    @GetMapping("/algorithm")
    public ResponseEntity<String> getBubbleSortedNumbers(@RequestParam(name = "arr") Integer[] intArray) {
        bubbleSort(intArray, intArray.length);
        return ResponseEntity.ok(Arrays.stream(intArray).map(Object::toString).collect(Collectors.joining(",")));
    }

    private static void bubbleSort(Integer[] arr, Integer n) {
        int i, j, temp;
        boolean swapped;
        for (i = 0; i < n - 1; i++) {
            swapped = false;
            for (j = 0; j < n - i - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                    swapped = true;
                }
            }
            if (!swapped)
                break;
        }
    }

    private static int partition(Integer[] arr, int low, int high)
    {

        // pivot
        int pivot = arr[high];

        // Index of smaller element and
        // indicates the right position
        // of pivot found so far
        int i = (low - 1);

        for(int j = low; j <= high - 1; j++)
        {

            // If current element is smaller
            // than the pivot
            if (arr[j] < pivot)
            {

                // Increment index of
                // smaller element
                i++;
                swap(arr, i, j);
            }
        }
        swap(arr, i + 1, high);
        return (i + 1);
    }

    private static void quickSort(Integer[] arr, int low, int high)
    {
        if (low < high)
        {

            // pi is partitioning index, arr[p]
            // is now at right place
            int pi = partition(arr, low, high);

            // Separately sort elements before
            // partition and after partition
            quickSort(arr, low, pi - 1);
            quickSort(arr, pi + 1, high);
        }
    }

    static void swap(Integer[] arr, int i, int j)
    {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

}





